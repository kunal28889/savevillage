Candy.Game = function(game){
	// define needed variables for Candy.Game
	this._player = null;
	this._candyGroup = null;
	this._spawnCandyTimer = 0;
	this._spawnBombTimer = 0;
	this._fontStyle = null;

	Candy.curLvl = "Level " + curLvl;
	Candy._lives = [3];
	// define Candy variables to reuse them in Candy.item functions
	Candy._scoreText = null;
	Candy._score = 0;
	Candy._health = 0;
};
Candy.Game.prototype = {
	create: function(){
		// start the physics engine
		this.physics.startSystem(Phaser.Physics.ARCADE);
		// set the global gravity
		this.physics.arcade.gravity.y = 200;
		// display images: background, floor and score
		this.add.sprite(0, 0, 'background');
		// create new group for candy
		this._candyGroup = this.add.group();
		// this.add.sprite(-30, Candy.GAME_HEIGHT- 91, 'floor');
		this.add.sprite(10, 5, 'score-bg');
		// add pause button
		// this.add.button(Candy.GAME_WIDTH-96-10, 5, 'button-pause', this.managePause, this);

		// footer.style.display = "block";
		// create the player
		var h = Candy.GAME_HEIGHT- (myVars.monsterIdle.height);
		this._player = this.add.sprite(5, h, 'monster-idle');
		// add player animation
		// this._player.animations.add('idle', [0,1,2,3,4,5,6,7,8,9,10,11], 10, true);
		// play the animation
		// this._player.animations.play('idle');
		// set font style
		this._fontStyle = { font: "43px Arial", fill: "#ff880a", stroke: "#ffffff", strokeThickness: 2, align: "center" };
		// initialize the spawn timer
		this._spawnCandyTimer = 0;
		// initialize the score text with 0
		Candy._scoreText = this.add.text(150, 16, "0", this._fontStyle);
		// set health of the player
		Candy._health = 30;
		// spawn first candy
		Candy.item.spawnCandy(this);

		this.input.onDown.add(function (argument) {
			 // this._candyGroup.forEach(this.physics.arcade.moveToPointer, this.physics.arcade, false, 200);
		}, this);

		Candy._lives[0] = this.add.sprite(Candy.GAME_WIDTH - myVars.life.width, Candy.GAME_HEIGHT - myVars.life.height, "life1")
		Candy._lives[1] = this.add.sprite(Candy.GAME_WIDTH - myVars.life.width*2, Candy.GAME_HEIGHT - myVars.life.height, "life1")
		Candy._lives[2] = this.add.sprite(Candy.GAME_WIDTH - myVars.life.width*3, Candy.GAME_HEIGHT - myVars.life.height, "life1")

	},
	managePause: function(){
		// pause the game
		this.game.paused = true;
		// add proper informational text
		var pausedText = this.add.text(100, 250, "Game paused.\nTap anywhere to continue.", this._fontStyle);
		// set event listener for the user's click/tap the screen
		this.input.onDown.add(function(){
			// remove the pause text
			pausedText.destroy();
			// unpause the game
			this.game.paused = false;
		}, this);
	},
	update: function(){
		// update timer every frame
		this._spawnCandyTimer += this.time.elapsed;
		// if spawn timer reach one second (1000 miliseconds)
		if(this._spawnCandyTimer > (900 - (curLvl*100))) {
			// reset it
			this._spawnCandyTimer = 0;
			// and spawn new candy
			Candy.item.spawnCandy(this);
		}
		// loop through all candy on the screen
		this._candyGroup.forEach(function(candy){
			// to rotate them accordingly
			candy.angle += candy.rotateMe;
		});

		// update timer every frame
		this._spawnBombTimer += this.time.elapsed;
		// if spawn timer reach one second (1000 miliseconds)
		if(this._spawnBombTimer > (2000 - (curLvl*200)) && (curLvl > 1)) {
			// reset it
			this._spawnBombTimer = 0;
			// and spawn new candy
			Candy.item.spawnBomb(this);
		}

		// loop through all candy on the screen
		this._candyGroup.forEach(function(candy){
			// to rotate them accordingly
			candy.angle += candy.rotateMe;
		});
		// if the health of the player drops to 0, the player dies = game over
		if(!Candy._health) {
			// show the game over message
			this.add.sprite((Candy.GAME_WIDTH - myVars.gameOver.width)/2, (Candy.GAME_HEIGHT - myVars.gameOver.height)/2, 'game-over');
			// pause the game
			this.game.paused = true;
			this._player.kill();
		}

	}
};

Candy.item = {
	spawnCandy: function(game){
		// calculate drop position (from 0 to game width) on the x axis
		var dropPos = Math.floor(Math.random()*((Candy.GAME_WIDTH - 80)-80+1)+80);
		//Math.floor(Math.random()*Candy.GAME_WIDTH);
		// define the offset for every candy
		var dropOffset = [-27,-36,-36,-38,-48];
		// randomize candy type
		var candyType = Math.floor(Math.random()*5);
		// create new candy
		var candy = game.add.sprite(dropPos, dropOffset[candyType], 'candy');
		candy.got = false;
		// add new animation frame
		candy.animations.add('anim', [candyType], 10, true);
		// play the newly created animation
		candy.animations.play('anim');
		// enable candy body for physic engine
		game.physics.enable(candy, Phaser.Physics.ARCADE);

		

		// enable candy to be clicked/tapped
		candy.inputEnabled = true;
		// add event listener to click/tap
		candy.events.onInputDown.add(this.clickCandy);
		// be sure that the candy will fire an event when it goes out of the screen
		candy.checkWorldBounds = true;
		// reset candy when it goes out of screen
		candy.events.onOutOfBounds.add(this.removeCandy, this);
		// set the anchor (for rotation, position etc) to the middle of the candy
		candy.anchor.setTo(0.5, 0.5);
		// set the random rotation value
		candy.rotateMe = (Math.random()*4)-2;
		// add candy to the group
		game._candyGroup.add(candy);
	},
	clickCandy: function(candy, pointer){
		// kill the candy when it's clicked
		// candy.scale.set(0.5);
		// add points to the score
		Candy._score += 1;
		// update score text
		Candy._scoreText.setText(Candy._score);

		game.physics.arcade.moveToXY(candy, 10, winHeight-20, 30, 200);
		candy.got = true;

		switch(Candy._score){
			case 10 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
			case 30 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
			case 40 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
			case 60 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
			case 80 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
			case 100 : curLvl++;
						game.physics.arcade.gravity.y = curLvl*200;
				break;
		}
	},
	removeCandy: function(candy){
		// kill the candy
		candy.kill();
		// decrease player's health
		if (candy.got) {}
			else {
			Candy._health -= 10;
			Candy._lives[Math.ceil((Candy._health-1)/10)].kill();
		}
	},
	spawnBomb: function(game){
		// calculate drop position (from 0 to game width) on the x axis
		var dropPos = Math.floor(Math.random()*((Candy.GAME_WIDTH - 80)-80+1)+80);
		//Math.floor(Math.random()*Candy.GAME_WIDTH);
		// create new bomb
		var bomb = game.add.sprite(dropPos, -50, 'bomb');
		// enable candy body for physic engine
		game.physics.enable(bomb, Phaser.Physics.ARCADE);
		bomb.body.gravity.y = curLvl*100;
		// enable candy to be clicked/tapped
		bomb.inputEnabled = true;
		// add event listener to click/tap
		bomb.events.onInputDown.add(this.clickBomb);
		// be sure that the candy will fire an event when it goes out of the screen
		bomb.checkWorldBounds = true;
		// reset candy when it goes out of screen
		bomb.events.onOutOfBounds.add(this.removeBomb, this);
		// set the anchor (for rotation, position etc) to the middle of the candy
		bomb.anchor.setTo(0.5, 0.5);
		// set the random rotation value
		bomb.rotateMe = (Math.random()*4)-2;
		// add candy to the group
		game._candyGroup.add(bomb);
	},
	clickBomb: function(bomb, pointer){

		game.physics.arcade.moveToXY(bomb, 10, winHeight-20, 30, 200);
		bomb.kill();
		Candy._health = 0;
	},
	removeBomb: function(bomb){
		// kill the candy
		bomb.kill();
	}
};
